<?php
/**
 * The bootstrap file creates and returns the container.
 */

use DI\ContainerBuilder;
use function DI\autowire;

ini_set('display_errors', 1);
$autoload = require __DIR__ . '/../vendor/autoload.php';

$containerBuilder = new ContainerBuilder;
$containerBuilder->addDefinitions(__DIR__ . '/config.php');
$defArray = [];
foreach ($autoload->getClassMap() as $key => $value) {
    $defArray[$key] = [autowire($key)];
}
$containerBuilder->addDefinitions();
$container = $containerBuilder->build();

return $container;
