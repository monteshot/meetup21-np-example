<?php

use Perspective\Novaposhta\Controller\AbstractController;
use Perspective\Novaposhta\Controller\WarehouseJsonController;
use function DI\create;
use function DI\autowire;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

return [
    // Bind an interface to an implementation
    //  ArticleRepository::class => create(InMemoryArticleRepository::class),
    // Configure Twig
    Environment::class => function () {
        $loader = new FilesystemLoader(__DIR__ . '/../src/Perspective/Magento/view');
        return new Environment($loader);
    },
   // WarehouseJsonController::class => autowire(WarehouseJsonController::class),
   // AbstractController::class => autowire(AbstractController::class),
];
