<?php


namespace Perspective\Magento\Model;


/**
 * Class OrderSoapXmlHelperModel
 */
class OrderSoapXmlHelperModel extends AbstractHelperModel
{
    /**
     * @return array|false|mixed|string|string[]
     */
    public function getApiUrl()
    {
        return str_replace('{{scopeSoap}}', $this->enviroment['scopeSoap'], $this->enviroment['apiUrl'] . 'soap/{{scopeSoap}}?wsdl&services=salesOrderRepositoryV1') ?? false;
    }

    /**
     * @return array|false|string|string[]
     */
    public function getAuthorizationUrl()
    {
        return str_replace('{{scopeSoap}}', $this->enviroment['scopeSoap'], $this->enviroment['apiUrl'] . 'soap?wsdl=1&services=integrationAdminTokenServiceV1') ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getUsername()
    {
        return $this->enviroment['soapUsername'] ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getPassword()
    {
        return $this->enviroment['soapPassword'] ?? false;
    }
}