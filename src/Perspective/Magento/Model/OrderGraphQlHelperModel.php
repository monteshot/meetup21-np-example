<?php


namespace Perspective\Magento\Model;


/**
 * Class OrderGraphQlHelperModel
 */
class OrderGraphQlHelperModel extends AbstractHelperModel
{
    /**
     * @return false|string
     */
    public function getApiUrl()
    {
        return $this->enviroment['apiUrl'] . 'graphql' ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getScope()
    {
        return $this->enviroment['scope'] ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getUsername()
    {
        return $this->enviroment['graphqlUsername'] ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getPassword()
    {
        return $this->enviroment['graphqlPassword'] ?? false;
    }
}