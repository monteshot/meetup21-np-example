<?php


namespace Perspective\Magento\Model;

/**
 * Class AbstractHelperModell
 */
abstract class AbstractHelperModel
{
    /**
     * @var array
     */
    public $enviroment;

    /**
     * AbstractHelperModel constructor.
     */
    public function __construct()
    {
        $enviroment = require dirname(__FILE__) . '../../../../../app/env.php';
        if (is_array($enviroment)) {
            $this->setEnviroment($enviroment);
        }
    }

    /**
     * @return false|mixed
     */
    public function getApiUrl()
    {
        return $this->enviroment['apiUrl'] ?? false;
    }

    /**
     * @return array
     */
    public function getEnviroment(): ?array
    {
        return $this->enviroment;
    }

    /**
     * @param array $enviroment
     */
    public function setEnviroment(array $enviroment): void
    {
        $this->enviroment = $enviroment;
    }

    /**
     * @return false
     */
    public function getUsername()
    {
        return false;
    }

    /**
     * @return false
     */
    public function getPassword()
    {
        return false;
    }

}