<?php


namespace Perspective\Magento\Model;


/**
 * Class OrderRestJsonHelperModel
 * @package Perspective\Magento\Model
 */
class OrderRestJsonHelperModel extends AbstractHelperModel
{
    /**
     * @param string $format
     * @return false|string
     */
    public function getApiUrl($format = 'json')
    {
        return $this->enviroment['apiUrl'] . 'rest/{{scope}}/V1/orders/{{id}}' ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getScope()
    {
        return $this->enviroment['scope'] ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getUsername()
    {
        return $this->enviroment['restUsername'] ?? false;
    }

    /**
     * @return false|mixed
     */
    public function getPassword()
    {
        return $this->enviroment['restPassword'] ?? false;
    }
}