<?php


namespace Perspective\Magento\Model;

class AuthorizationHelperModel extends AbstractHelperModel
{
    /**
     * @return false|string
     */
    public function getApiUrl()
    {
        return $this->getEnviroment()['apiUrl'] . 'rest/V1/integration/admin/token' ?? false;
    }

}