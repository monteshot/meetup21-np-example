<?php

namespace Perspective\Magento\Controller;

use Perspective\Magento\Model\HTTP\Client\Curl;
use Perspective\Magento\Model\AuthorizationHelperModel;
use Perspective\Magento\Model\HTTP\Client\GraphQL;
use Perspective\Magento\Model\OrderGraphQlHelperModel;
use Perspective\Magento\Model\Timer;
use Perspective\Magento\Model\OrderRestJsonHelperModel;
use stdClass;
use Twig\Environment;

/**
 * Class OrderGraphQlController
 */
class OrderGraphQlController extends AbstractRestController
{
    /**
     * @var \Perspective\Magento\Model\Timer
     */
    private $timer;
    /**
     * @var \Perspective\Magento\Model\HTTP\Client\Curl
     */
    private $curl;
    /**
     * @var \Perspective\Magento\Model\OrderRestJsonHelperModel
     */
    private $orderRestJsonHandler;
    /**
     * @var \Perspective\Magento\Model\HTTP\Client\GraphQL
     */
    private $graphQLClient;
    /**
     * @var \Perspective\Magento\Model\OrderGraphQlHelperModel
     */
    private $graphQlHelperModel;

    /**
     * WarehouseJsonController constructor.
     * @param \Twig\Environment $twig
     * @param \Perspective\Magento\Model\Timer $timer
     * @param \Perspective\Magento\Model\HTTP\Client\GraphQL $graphQLClient
     * @param \Perspective\Magento\Model\OrderGraphQlHelperModel $graphQlHelperModel
     */
    public function __construct(
        Environment $twig,
        Timer $timer,
        Curl $curl,
        OrderRestJsonHelperModel $orderRestJsonHelperModel,
        AuthorizationHelperModel $authorizationHandle,
        GraphQL $graphQLClient,
        OrderGraphQlHelperModel $graphQlHelperModel
    ) {
        parent::__construct($twig, $curl, $orderRestJsonHelperModel, $authorizationHandle);
        $this->timer = $timer;
        $this->graphQLClient = $graphQLClient;
        $this->twig = $twig;
        $this->graphQlHelperModel = $graphQlHelperModel;
    }


    /**
     * @param $id
     * @throws \ErrorException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function execute($id)
    {
        $this->timer->start();
        $result3 = $this->makeRequest($id);
        $timer3 = $this->timer->result();
        echo $this->twig->render('graphql.twig',
            [
                'timer3' => $timer3,
                'arrayResult3' => json_encode($result3),

            ]);
    }

    /**
     * @return string
     */
    public function authorizeGraphQL()
    {
        if ($token = $this->getAuthorizationToken()) {
            return $token;
        }
        $query = <<<'GRAPHQL'
    mutation {
        generateCustomerToken(email: {{$email}}, password: {{$password}}) {
            token
        }
    }
GRAPHQL;
        $params = [];
        $username = $this->graphQlHelperModel->getUsername();
        $password = $this->graphQlHelperModel->getPassword();
        $query = str_replace('{{$email}}', "\"$username\"", $query);
        $query = str_replace('{{$password}}', "\"$password\"", $query);
        $result = $this->graphQLClient->makeRequest(
            $this->graphQlHelperModel->getApiUrl(),
            $query,
            $params
        );
        $this->setAuthorizationToken($result['data']['generateCustomerToken']['token'] ?? null);
        // $this->authorizeRest(); - для админа
        return $this->getAuthorizationToken();
    }


    /**
     * @return array
     * @throws \ErrorException
     */
    private function makeRequest()
    {
        $args = func_get_args();
        $orderId = reset($args) ?? '000000001';
        $query = <<<'GRAPHQL'
 query {
  customer {
    orders(filter: {number: {match: {{orderId}}}}) {
      total_count
      items {
        id
        number
        order_date
        status
        items {
          product_name
          product_sku
          product_url_key
          product_sale_price {
            value
          }
          product_sale_price {
            value
            currency
          }
          quantity_ordered
          quantity_invoiced
          quantity_shipped
        }
        carrier
        shipments {
          id
          number
          items {
            product_name
            quantity_shipped
          }
        }
        total {
          base_grand_total {
            value
            currency
          }
          grand_total {
            value
            currency
          }
          total_tax {
            value
          }
          subtotal {
            value
            currency
          }
          taxes {
            amount {
              value
              currency
            }
            title
            rate
          }
          total_shipping {
            value
          }
          shipping_handling {
            amount_including_tax {
              value
            }
            amount_excluding_tax {
              value
            }
            total_amount {
              value
            }
            taxes {
              amount {
                value
              }
              title
              rate
            }
          }
          discounts {
            amount {
              value
              currency
            }
            label
          }
        }
      }
    }
  }
 }
GRAPHQL;
        $query = str_replace('{{orderId}}', "\"$orderId\"", $query);
        //$query = str_replace('{{orderId}}', "\"000000001\"", $query);
        $params = [];
        $result = $this->graphQLClient->makeRequest(
            $this->graphQlHelperModel->getApiUrl(),
            $query,
            $params,
            $this->authorizeGraphQL()
        );
        return $result;
    }
}
