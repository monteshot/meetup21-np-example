<?php

namespace Perspective\Magento\Controller;

use Perspective\Magento\Model\HTTP\Client\Curl;
use Perspective\Magento\Model\AuthorizationHelperModel;
use Perspective\Magento\Model\OrderSoapXmlHelperModel;
use Perspective\Magento\Model\Timer;
use Perspective\Magento\Model\OrderRestJsonHelperModel;
use SoapClient;
use stdClass;
use Twig\Environment;

/**
 * Class OrderSoapXmlController
 */
class OrderSoapXmlController
{
    /**
     * @var \Perspective\Magento\Model\Timer
     */
    private $timer;
    /**
     * @var \Perspective\Magento\Model\HTTP\Client\Curl
     */
    private $curl;
    /**
     * @var \Perspective\Magento\Model\OrderRestJsonHelperModel
     */
    private $orderRestJsonHandler;
    /**
     * @var \Twig\Environment
     */
    private $twig;
    /**
     * @var \Perspective\Magento\Model\OrderSoapXmlHelperModel
     */
    private $orderSoapXmlHelperModel;
    /**
     * @var string
     */
    private $authorizationToken = '';

    /**
     * OrderSoapXmlController constructor.
     * @param \Twig\Environment $twig
     * @param \Perspective\Magento\Model\Timer $timer
     * @param \Perspective\Magento\Model\OrderSoapXmlHelperModel $orderSoapXmlHelperModel
     */
    public function __construct(
        Environment $twig,
        Timer $timer,
        OrderSoapXmlHelperModel $orderSoapXmlHelperModel
    ) {
        $this->timer = $timer;
        $this->twig = $twig;
        $this->orderSoapXmlHelperModel = $orderSoapXmlHelperModel;
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function execute($id)
    {
        $this->timer->start();
        $currentToken = $this->getToken();
        $opts = [
            'http' => [
                'user_agent' => 'PHPSoapClient',
                'header' => 'Authorization: Bearer ' . $this->getAuthorizationToken()
            ]
        ];
        $context = stream_context_create($opts);
        $serviceArgs = array('searchCriteria' =>
            array('filterGroups' =>
                array('filters' =>
                    array('field' => 'increment_id',
                        'value' => $id ?? '000000001',
                        'condition_type' => 'eq')
                )
            )
        );
        $soapClient = new SoapClient(
            $this->orderSoapXmlHelperModel->getApiUrl(),
            [
                'stream_context' => $context
            ]
        );
        $result2 = $soapClient->salesOrderRepositoryV1GetList($serviceArgs);
        $timer2 = $this->timer->result();
        echo $this->twig->render('xml.twig',
            [
                'timer2' => $timer2,
                'arrayResult2' => json_encode($result2)
            ]
        );
    }

    /**
     * @return string
     */
    public function getAuthorizationToken(): string
    {
        return $this->authorizationToken;
    }

    /**
     * @param string $authorizationToken
     */
    public function setAuthorizationToken(string $authorizationToken): void
    {
        $this->authorizationToken = $authorizationToken;
    }

    /**
     * @return string
     * @throws \SoapFault
     */
    private function getToken()
    {
        if (!empty($this->getAuthorizationToken())) {
            return $this->getAuthorizationToken();
        }
        $opts = [
            'http' => [
                'user_agent' => 'PHPSoapClient'
            ]
        ];
        $request = new SoapClient($this->orderSoapXmlHelperModel->getAuthorizationUrl(), ([
            'stream_context' => stream_context_create($opts)
        ]));
        $token = $request->integrationAdminTokenServiceV1CreateAdminAccessToken(
            [
                "username" => $this->orderSoapXmlHelperModel->getUsername(),
                "password" => $this->orderSoapXmlHelperModel->getPassword()
            ]
        );

        if (!empty($token->result)) {
            $this->setAuthorizationToken($token->result);
        }
        return $this->getAuthorizationToken();
    }
}
