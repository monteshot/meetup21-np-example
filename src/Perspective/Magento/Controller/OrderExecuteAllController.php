<?php


namespace Perspective\Magento\Controller;

use Twig\Environment;

class OrderExecuteAllController
{
    /**
     * @var \Perspective\Magento\Controller\OrderSoapXmlController
     */
    private $orderSoapXmlController;
    /**
     * @var \Perspective\Magento\Controller\OrderRestJsonRestController
     */
    private $orderRestJsonRestController;
    /**
     * @var \Perspective\Magento\Controller\OrderGraphQlController
     */
    private $orderGraphQlController;
    /**
     * @var \Twig\Environment
     */
    private $twig;

    /**
     * OrderExecuteAllController constructor.
     * @param \Perspective\Magento\Controller\OrderSoapXmlController $orderSoapXmlController
     * @param \Perspective\Magento\Controller\OrderRestJsonRestController $orderRestJsonRestController
     * @param \Perspective\Magento\Controller\OrderGraphQlController $orderGraphQlController
     * @param \Twig\Environment $twig
     */
    public function __construct(
        OrderSoapXmlController $orderSoapXmlController,
        OrderRestJsonRestController $orderRestJsonRestController,
        OrderGraphQlController $orderGraphQlController,
        Environment $twig
    ) {
        $this->orderSoapXmlController = $orderSoapXmlController;
        $this->orderRestJsonRestController = $orderRestJsonRestController;
        $this->orderGraphQlController = $orderGraphQlController;
        $this->twig = $twig;
    }

    public function execute($id = "000000001")
    {
        $specificId = $id;
        $html = [];
        ob_start();
        $this->orderRestJsonRestController->execute($specificId);
        $html[] = ob_get_clean();
        ob_start();
        $this->orderSoapXmlController->execute($specificId);
        $html[] = ob_get_clean();
        ob_start();
        $this->orderGraphQlController->execute($specificId);
        $html[] = ob_get_clean();
        echo implode('', $html);
        /*echo $this->twig->render('allApi.twig', [
                'html1' => $html['0'],
                'html2' => $html['1'],
                'html3' => $html['2'],
                ]

        );*/

    }

}