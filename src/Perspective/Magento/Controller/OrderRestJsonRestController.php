<?php

namespace Perspective\Magento\Controller;

use Perspective\Magento\Model\HTTP\Client\Curl;
use Perspective\Magento\Model\AuthorizationHelperModel;
use Perspective\Magento\Model\Timer;
use Perspective\Magento\Model\OrderRestJsonHelperModel;
use stdClass;
use Twig\Environment;

/**
 * Class OrderRestJsonRestController
 */
class OrderRestJsonRestController extends AbstractRestController
{
    /**
     * @var \Perspective\Magento\Model\Timer
     */
    private $timer;
    /**
     * @var \Perspective\Magento\Model\HTTP\Client\Curl
     */
    private $curl;
    /**
     * @var \Perspective\Magento\Model\OrderRestJsonHelperModel
     */
    private $orderRestJsonHandler;

    /**
     * WarehouseJsonController constructor.
     * @param \Twig\Environment $twig
     * @param \Perspective\Magento\Model\Timer $timer
     * @param \Perspective\Magento\Model\HTTP\Client\Curl $curl
     * @param \Perspective\Magento\Model\OrderRestJsonHelperModel $orderRestJsonHandler
     * @param \Perspective\Magento\Model\AuthorizationHelperModel $authorizationHandle
     */
    public function __construct(
        Environment $twig,
        Timer $timer,
        Curl $curl,
        OrderRestJsonHelperModel $orderRestJsonHandler,
        AuthorizationHelperModel $authorizationHandle
    ) {
        parent::__construct($twig, $curl, $orderRestJsonHandler, $authorizationHandle);
        $this->timer = $timer;
        $this->curl = $curl;
        $this->orderRestJsonHandler = $orderRestJsonHandler;
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function execute($id)
    {
        $uri = $this->prepareURI($id);
        $this->timer->start();
        $jsonParams = $this->prepareCurl();
        $result1 = $this->performCurl($uri, $jsonParams);
        $timer1 = $this->timer->result();
        echo $this->twig->render('json.twig',
            [
                'timer1' => $timer1,
                'arrayResult1' => $result1,

            ]);
    }

    /**
     * @return array|string|string[]
     */
    private function prepareURI()
    {
        $uri = '';
        if ($args = func_get_args()) {
            $uri = str_replace('{{id}}', reset($args), $this->orderRestJsonHandler->getApiUrl());
        } else {
            $uri = str_replace('{{id}}', '000000001', $this->orderRestJsonHandler->getApiUrl());
        }
        $uri = str_replace('{{scope}}', $this->orderRestJsonHandler->getScope(), $uri);
        return $uri;
    }

    /**
     * @param $uri
     * @param $params
     * @return mixed|string
     */
    protected function performCurl($uri, $params)
    {
        if (empty($this->getAuthorizationToken())) {
            $this->authorizeRest();
        }
        $this->curl->setOption(CURLOPT_URL, $uri);
        $this->curl->setOption(CURLOPT_RETURNTRANSFER, 1);
        $this->curl->setOption(CURLOPT_CUSTOMREQUEST, "GET");
        $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
        $this->curl->setOption(CURLOPT_CONNECTTIMEOUT, 300);
        $headers = [
            "Content-Type: application/json",
            "Authorization: Bearer " . json_decode($this->getAuthorizationToken())
        ];
        $this->curl->setOption(CURLOPT_HTTPHEADER, $headers);
        $this->curl->post($uri, []);
        return $this->curl->getBody();
    }
}
