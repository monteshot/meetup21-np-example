<?php


namespace Perspective\Magento\Controller;

use Perspective\Magento\Model\HTTP\Client\Curl;
use Perspective\Magento\Model\AuthorizationHelperModel;
use Perspective\Magento\Model\OrderRestJsonHelperModel;
use Twig\Environment;

/**
 * Class AbstractRestController
 */
abstract class AbstractRestController
{
    /**
     * @var \Twig\Environment
     */
    protected $twig;
    /**
     * @var string
     */
    protected $authorizationToken;
    /**
     * @var \Perspective\Magento\Model\HTTP\Client\Curl
     */
    private $curl;
    /**
     * @var \Perspective\Magento\Model\AuthorizationHelperModel
     */
    private $authorizationHandle;
    /**
     * @var \Perspective\Magento\Model\OrderRestJsonHelperModel
     */
    private $orderRestJsonHelperModel;

    /**
     * AbstractController constructor.
     * @param \Twig\Environment $twig
     */
    public function __construct(
        Environment $twig,
        Curl $curl,
        OrderRestJsonHelperModel $orderRestJsonHelperModel,
        AuthorizationHelperModel $authorizationHandle
    ) {
        $this->twig = $twig;
        $this->curl = $curl;
        $this->authorizationHandle = $authorizationHandle;
        $this->orderRestJsonHelperModel = $orderRestJsonHelperModel;
    }

    /**
     * @return string
     */
    public function getAuthorizationToken(): ?string
    {
        return $this->authorizationToken;
    }

    /**
     * @param string $authorizationToken
     */
    public function setAuthorizationToken(string $authorizationToken): void
    {
        $this->authorizationToken = $authorizationToken;
    }

    /**
     * @return mixed
     */
    protected function prepareCurl()
    {
        return [];
    }

    /**
     * @return mixed
     */
    protected function performCurl($uri, $params)
    {
        return '';
    }

    public function authorizeRest()
    {
        $uri = $this->authorizationHandle->getApiUrl();
        $params = [
            'username' => $this->orderRestJsonHelperModel->getUsername(),
            'password' => $this->orderRestJsonHelperModel->getPassword()
        ];
        $this->curl->setOption(CURLOPT_URL, $uri);
        $this->curl->setOption(CURLOPT_POSTFIELDS, json_encode($params));
        $this->curl->setOption(CURLOPT_RETURNTRANSFER, 1);
        $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
        $this->curl->setOption(CURLOPT_CONNECTTIMEOUT, 300);
        $this->curl->addHeader('Content-Type', 'application/json');
        $this->curl->post($uri, []);
        $this->setAuthorizationToken($this->curl->getBody());
    }
}