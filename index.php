<?php

use FastRoute\RouteCollector;
use function DI\create;

$container = require __DIR__ . '/app/bootstrap.php';

$dispatcher = FastRoute\simpleDispatcher(function (RouteCollector $r) {
    $r->addRoute('GET', '/rest/{id}', ['Perspective\Magento\Controller\OrderRestJsonRestController', 'execute']);
    $r->addRoute('GET', '/rest/', ['Perspective\Magento\Controller\OrderRestJsonRestController', 'execute']);
    $r->addRoute('GET', '/soap/{id}', ['Perspective\Magento\Controller\OrderSoapXmlController', 'execute']);
    $r->addRoute('GET', '/soap/', ['Perspective\Magento\Controller\OrderSoapXmlController', 'execute']);
    $r->addRoute('GET', '/graphql/{id}', ['Perspective\Magento\Controller\OrderGraphQlController', 'execute']);
    $r->addRoute('GET', '/graphql/', ['Perspective\Magento\Controller\OrderGraphQlController', 'execute']);
    $r->addRoute('GET', '/{id}', ['Perspective\Magento\Controller\OrderExecuteAllController', 'execute']);
    $r->addRoute('GET', '/', ['Perspective\Magento\Controller\OrderExecuteAllController', 'execute']);

});

$route = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

switch ($route[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo '404 Not Found';
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        echo '405 Method Not Allowed';
        break;

    case FastRoute\Dispatcher::FOUND:
        $controller = $route[1];
        $parameters = $route[2];

        // We could do $container->get($controller) but $container->call()
        // does that automatically
        $container->call($controller, $parameters);
        break;
}
